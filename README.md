**Movies api**

## General info
Simple REST API for collect currency rates from ucb.

## Technologies
- Django 
- Django Rest Framework

## Requirements:
- docker
- docker-compose
- git

## Setup
- `git clone https://rkossak@bitbucket.org/rkossak/omdb-api-movies.git`
- `docker-compose build`
- `docker-compose up`
- `docker-compose run web ./manage.py migrate`
- `docker-compose run web ./manage.py currency_scraper`

Go to `http://0.0.0.0:8000/currency-rate/`