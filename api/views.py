from rest_framework import viewsets

from api.models import CurrencyRate
from api.serializers import CurrencyRateSerializer


class CurrencyRateViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CurrencyRate.objects.all()
    serializer_class = CurrencyRateSerializer
