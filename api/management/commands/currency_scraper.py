from django.core.management.base import BaseCommand

from api.models import CurrencyRate
from api.utils import country_rates


class Command(BaseCommand):
    help = 'Currency scraper'

    def handle(self, *args, **options):
        currency_rates, created = CurrencyRate.objects.get_or_create()
        if not created:
            print("## Currency rates for this day will be overwritten. ##")
        currency_rates.rates = country_rates()
        currency_rates.save()
