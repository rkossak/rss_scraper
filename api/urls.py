from rest_framework import routers

from api.views import CurrencyRateViewSet

router = routers.DefaultRouter()
router.register(r'currency-rate', CurrencyRateViewSet, base_name="currency_rate")
urlpatterns = router.urls
