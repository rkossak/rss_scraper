import datetime
from django.contrib.postgres.fields import JSONField
from django.db import models


class CurrencyRate(models.Model):
    base = models.CharField(max_length=3, default="EUR")
    date = models.DateField(default=datetime.date.today, unique=True)
    rates = JSONField(default=dict)
