import feedparser
from django.conf import settings


def country_rates():
    rates = {}
    for country_code in settings.COUNTRY_CODE_LIST:
        feed = feedparser.parse(
            settings.ECB_RSS_URL.format(
                country_code=country_code.lower()
            )
        )
        try:
            rate = feed['entries'][0]['cb_exchangerate'].split("\n")[0]
        except KeyError:
            continue
        else:
            rates[country_code] = rate
    return rates
